define(['N/record', 'N/https', 'N/ui/dialog'], (record, https, dialog) => {
    /**
     * Module Description...
     *
     * @type {Object} module-name
     *
     * @copyright 2021 Lion Energy, LLC
     * @author Steven Deshazer <steven@lionenergy.com>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount

     * @NScriptType ClientScript
     */
    const exports = {}
    let context

    /**
     * pageInit event handler; executed when the page completes loading or when the form is reset.
     *
     * @gov XXX
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.mode - The access mode of the current record.
     * @param {CurrentRecord} scriptContext.currentRecord - The record in context
     */
    exports.pageInit = (scriptContext) => {
        context = scriptContext
        const {currentRecord: cr} = context

        cr.getField({fieldId: "custpage_finish_btn"}).isDisabled = true
        cr.getField({fieldId: "custpage_pause_resume_btn"}).isDisabled = true

        // Hide unnecessary secondary buttons and the icons to add or view select options
        document.getElementsByClassName('uir-secondary-buttons')[0].style.display = 'none'
        // Array.from(document.getElementsByClassName('uir-field-widget')).forEach(el => el.style.display = 'none')
    }


    exports.startRepair = () => {
        const {currentRecord: cr} = context
        cr.getField({fieldId: "custpage_start_btn"}).isDisabled = true
        cr.getField({fieldId: "custpage_pause_resume_btn"}).isDisabled = false
        cr.getField({fieldId: "custpage_finish_btn"}).isDisabled = false
        cr.setValue({fieldId: "custpage_start_time", value: new Date()})
        cr.setValue({fieldId: "custpage_status", value: "IN PROGRESS"})
    }

    exports.pauseOrResumeRepair = () => {
        const {currentRecord: cr} = context
        const currentStatus = cr.getValue({fieldId: "custpage_status"})
        const currentPauseTime = cr.getValue({fieldId: "custpage_pause_time"})

        console.log(currentStatus, currentPauseTime)

        if (currentStatus === "PAUSED" || currentPauseTime) {
            // Resume repair
            cr.setValue({fieldId: "custpage_pause_time", value: null})
            document.getElementById("custpage_pause_resume_btn").value = "Pause"
            cr.setValue({fieldId: "custpage_status", value: "IN PROGRESS"})
            const currentDownTime = Number(cr.getValue({fieldId: "custpage_down_time"}))
            const newDownTime = currentDownTime + (new Date() - currentPauseTime)
            cr.setValue({fieldId: "custpage_down_time", value: newDownTime})

        } else {
            // Pause repair
            document.getElementById("custpage_pause_resume_btn").value = "Resume"
            cr.setValue({fieldId: "custpage_pause_time", value: new Date()})
            cr.setValue({fieldId: "custpage_status", value: "PAUSED"})
        }
    }

    /**
     * @param {Record} curRec
     * @returns Boolean
     */
    function validateForm(curRec) {
        const requiredFields = [
            'custpage_tester',
            'custpage_item',
            'custpage_serial_num',
            'custpage_fault',
            'custpage_repair_work_done',
            'custpage_repair_result'
        ]
        for(let fieldId of requiredFields) {
            if(!curRec.getValue({fieldId})) {
                dialog.alert({title: "But wait...", message: "Missing required field(s)!"})
                return false
            }
        }
        return true
    }


    exports.finishRepair = () => {
        const {currentRecord: cr} = context

        if (!validateForm(cr)) return

        cr.getField({fieldId: "custpage_pause_resume_btn"}).isDisabled = true
        cr.getField({fieldId: "custpage_finish_btn"}).isDisabled = true
        cr.setValue({fieldId: "custpage_status", value: "SAVING..."})

        const currentStatus = cr.getValue({fieldId: "custpage_status"})
        const currentPauseTime = cr.getValue({fieldId: "custpage_pause_time"})

        if (currentStatus === "PAUSED" || currentPauseTime) {
            // This means we've gone straight from PAUSED to FINISHED
            const currentDownTime = Number(cr.getValue({fieldId: "custpage_down_time"}))
            const newDownTime = currentDownTime + (new Date() - currentPauseTime)
            cr.setValue({fieldId: "custpage_down_time", value: newDownTime})
        }
        const downTime = Number(cr.getValue({fieldId: "custpage_down_time"}))

        // Save record and refresh page on success
        const postResp = https.post({
            url: location.href,
            body: JSON.stringify({
                repairTime: new Date() - cr.getValue({fieldId: "custpage_start_time"}),
                downTime: downTime,
                employee: cr.getValue({fieldId: "custpage_tester"}),
                item: cr.getValue({fieldId: "custpage_item"}),
                serial: cr.getValue({fieldId: "custpage_serial_num"}),
                fault: cr.getValue({fieldId: "custpage_fault"}),
                workDone: cr.getValue({fieldId: "custpage_repair_work_done"}),
                result: cr.getValue({fieldId: "custpage_repair_result"}),
                notes: cr.getValue({fieldId: "custpage_notes"}),
                refNum: cr.getValue({fieldId: "custpage_ref_num"})
            }),
            headers: {}
        })
        dialog.alert({title: "Save Result", message: postResp.body}).then(() => {
            const body = JSON.parse(postResp.body)
            if (body.status_code === 200) window.location.reload()
        }).catch(e => {
            throw e
        })
    }

    return exports
})
