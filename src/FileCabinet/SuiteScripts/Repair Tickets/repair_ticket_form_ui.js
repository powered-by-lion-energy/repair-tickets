/**
 * @NApiVersion 2.1
 * @NModuleScope SameAccount
 */
define(['N/ui/serverWidget', 'N/record'],
    /**
     * @param{serverWidget} ui
     * @param{record} record
     */
    function (ui, record) {

        function renderPage() {
            const form = ui.createForm({title: "Create Repair Ticket"})
            form.clientScriptModulePath = './repair_ticket_client.js'
            renderFields(form)
            renderButtons(form)
            return form
        }

        /**
         * @param {Form} form
         */
        function renderFields(form) {
            form.addField({
                id: "custpage_status",
                type: ui.FieldType.TEXT,
                label: "Status",
            })
                .updateDisplayType({displayType: ui.FieldDisplayType.INLINE})
                .updateBreakType({breakType: ui.FieldBreakType.STARTCOL})
                .defaultValue = "READY"
            form.addField({
                id: "custpage_start_time",
                type: ui.FieldType.DATETIMETZ,
                label: "Start Time"
            }).updateDisplayType({displayType: ui.FieldDisplayType.INLINE})
            form.addField({
                id: "custpage_tester",
                type: ui.FieldType.SELECT,
                source: record.Type.EMPLOYEE,
                label: "Tester"
            }).isMandatory = true
            form.addField({
                id: "custpage_item",
                type: ui.FieldType.SELECT,
                source: record.Type.INVENTORY_ITEM,
                label: "Item"
            }).isMandatory = true
            form.addField({
                id: "custpage_serial_num",
                type: ui.FieldType.TEXT,
                label: "Serial Number"
            }).isMandatory = true
            form.addField({
                id: "custpage_fault",
                type: ui.FieldType.SELECT,
                source: 'customlist_repair_faults',
                label: "Fault"
            }).isMandatory = true
            form.addField({
                id: "custpage_repair_work_done",
                type: ui.FieldType.SELECT,
                source: 'customlist_repair_work_done',
                label: "Work Done"
            }).isMandatory = true
            form.addField({
                id: "custpage_repair_result",
                type: ui.FieldType.SELECT,
                source: 'customlist_repair_results',
                label: "Result"
            }).isMandatory = true
            form.addField({
                id: "custpage_notes",
                type: ui.FieldType.LONGTEXT,
                label: "Notes"
            })
            form.addField({
                id: "custpage_ref_num",
                type: ui.FieldType.TEXT,
                label: "Reference Number"
            })
            form.addField({
                id: "custpage_pause_time",
                type: ui.FieldType.DATETIMETZ,
                label: "Pause Time"
            }).updateDisplayType({displayType: ui.FieldDisplayType.HIDDEN})
            form.addField({
                id: "custpage_down_time",
                type: ui.FieldType.INTEGER,
                label: "Down Time"
            }).updateDisplayType({displayType: ui.FieldDisplayType.HIDDEN})
        }

        /**
         * @param {Form} form
         */
        function renderButtons(form) {
            Array({
                id: "custpage_start_btn",
                label: "Start",
                functionName: "startRepair()"
            }, {
                id: "custpage_pause_resume_btn",
                label: "Pause",
                functionName: "pauseOrResumeRepair()"
            }, {
                id: "custpage_finish_btn",
                label: "Finish",
                functionName: "finishRepair()"
            }).forEach(btn => form.addButton(btn))
        }


        return {renderPage}
    }
)
