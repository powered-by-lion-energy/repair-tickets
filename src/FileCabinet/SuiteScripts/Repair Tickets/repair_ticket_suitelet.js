define(['N/https', 'N/runtime', 'N/error', 'N/record', './repair_ticket_form_ui'], (https, runtime, error, record, form_ui) => {
    /**
     * Module Description...
     *
     * @type {Object} module-name
     *
     * @copyright 2021 Lion Energy, LLC
     * @author Steven Deshazer <steven@lionenergy.com>
     *
     * @NApiVersion 2.1
     * @NModuleScope SameAccount
     * @NScriptType Suitelet
     */

    const tzOffset = 3600 * 1000 * -7
    const timeStamp = () => new Date(new Date().getTime() + tzOffset).toISOString()

    /**
     * onRequest event handler
     *
     * @gov XXX
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - The incoming request object
     * @param {ServerResponse} context.response - The outgoing response object
     */
    function onRequest(context) {
        const startTime = new Date()
        const method = context.request.method
        const currentScript = runtime.getCurrentScript()
        log.audit({title: method + " Request Received", details: method + " Request Received at " + timeStamp()})

        const eventRouter = {
            [https.Method.GET]: onGet,
            [https.Method.POST]: onPost
        }

        try {
            (eventRouter[method])(context)
        } catch (e) {
            // LOG ERRORS
            if (e instanceof Error) e = e.toString()
            log.error({title: method + " Request Error", details: JSON.stringify(e)})
            context.response.write({output: JSON.stringify({status_code: 500, result: "error", error: e})})
        } finally {
            const endTime = new Date()
            log.audit({
                title: method + " Request Complete",
                details: ` Request Complete at ` + timeStamp() + ` | Time Elapsed: ` +
                    `${(endTime - startTime) / 1000} s | Remaining Usage: ${currentScript.getRemainingUsage()}`
            })
        }
    }

    function onGet(context) {
        context.response.writePage({pageObject: form_ui.renderPage()})
    }

    function onPost(context) {
        const {request, response} = context
        const {body: bodyStr} = request
        const body = JSON.parse(bodyStr)
        const {repairTime, downTime, employee, item, serial, fault, workDone, result, notes, refNum} = body

        const repairTicketId = record.create({type: 'customrecord_repair_ticket'})
            .setValue({fieldId: "custrecord_repair_time", value: Math.round(repairTime/1000)})
            .setValue({fieldId: "custrecord_repair_down_time", value: Math.round(downTime/1000)})
            .setValue({fieldId: "custrecord_repair_tester", value: employee})
            .setValue({fieldId: "custrecord_repair_item", value: item})
            .setValue({fieldId: "custrecord_repair_serial", value: serial})
            .setValue({fieldId: "custrecord_repair_fault", value: fault})
            .setValue({fieldId: "custrecord_repair_work_done", value: workDone})
            .setValue({fieldId: "custrecord_repair_result", value: result})
            .setValue({fieldId: "custrecord_repair_notes", value: notes})
            .setValue({fieldId: "custrecord_repair_reference_num", value: refNum})
            .save()

        response.write({output: JSON.stringify({result: "success", status_code: 200, repairTicketId})})
    }

    return {onRequest}
})
